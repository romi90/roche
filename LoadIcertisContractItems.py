import sys


authParams = {
    "baseURL": "https://poc42-business-api.icertis.com/api",
    "version": "v1.0", 
    "IcmAuthTokenName": "IcmAuthToken",
    "IcmAuthTokenValue": "N/lNTCx/xXoANOMgwl9AndIekQkwY3k7NcVvKYMC1hknxSXCiMqnctvKJ2JQd8lo7VfTIREl6S4FaKf3N0V335nDBesBqZP+Ka8SUFKwpc/bARc3QeR0T17o0Aq7qujfHhz39YHecaeRTO0HqMuhIsC6Hj5dg9MBl9h+CjNEBBXZfSYMdIX2/UfE9GZ2BTFPrQQdS8tTpWWjQQ8LE6nkaRtNnS/wmOPfjDJjGMCDkjw3gWDB7Or+BrLp+OlB+ngb30VXHLAzTOgvX/mvzpi0jhQi2eNhaOJ9hf/oqPfkhwDZRYVt6l6J6M/Dr3JgBOgF8OLxkQkNyxoSPSm4KUiWmA==$Xo7m+C1j939vJjzu3T0t13Fssaux6fEoam4NuxNVWCF11fHpPpTmC8GK7aa78zasgteow0FiVDVCXWW3cTIYsRHkl3eIxjHjQUURQT3FyIIKlkEhKkrzMJEou5IsdzP8cQrQexffdwM4uxNw4PW2QNp4Rdw3LzI+lwi2q1jR/2E=$Tn2BUqJB5vaEoz3qbabz5a43LjDouBroNPpX17kbtAc/eeyCKEMAVXrBPL9mGgMeS28hwoBjkIxyqJchGBaaWw==$qNFvZWD8mF+MHl0pbosvV38Ekv/rDiwSPsQySbQxESg6clF8Edu7X1KUbTjY8LdBed8GlD7VaNwfw5ScnaQyRg==", 
    "contractTypeName": "ICMSalesagreement",
    "guid": "9bbb1f86-d1da-4e83-ae75-4b945c73ae06",
    "amendmentId": "9bbb1f86-d1da-4e83-ae75-4b945c73ae06", 
    "associationName": "ICMProductsandPrices",
    "agreementId": "9bbb1f86-d1da-4e83-ae75-4b945c73ae06"
}

headers =  {
    #"Content-Type": "application/json", 
    "IcmAuthToken": "N/lNTCx/xXoANOMgwl9AndIekQkwY3k7NcVvKYMC1hknxSXCiMqnctvKJ2JQd8lo7VfTIREl6S4FaKf3N0V335nDBesBqZP+Ka8SUFKwpc/bARc3QeR0T17o0Aq7qujfHhz39YHecaeRTO0HqMuhIsC6Hj5dg9MBl9h+CjNEBBXZfSYMdIX2/UfE9GZ2BTFPrQQdS8tTpWWjQQ8LE6nkaRtNnS/wmOPfjDJjGMCDkjw3gWDB7Or+BrLp+OlB+ngb30VXHLAzTOgvX/mvzpi0jhQi2eNhaOJ9hf/oqPfkhwDZRYVt6l6J6M/Dr3JgBOgF8OLxkQkNyxoSPSm4KUiWmA==$Xo7m+C1j939vJjzu3T0t13Fssaux6fEoam4NuxNVWCF11fHpPpTmC8GK7aa78zasgteow0FiVDVCXWW3cTIYsRHkl3eIxjHjQUURQT3FyIIKlkEhKkrzMJEou5IsdzP8cQrQexffdwM4uxNw4PW2QNp4Rdw3LzI+lwi2q1jR/2E=$Tn2BUqJB5vaEoz3qbabz5a43LjDouBroNPpX17kbtAc/eeyCKEMAVXrBPL9mGgMeS28hwoBjkIxyqJchGBaaWw==$qNFvZWD8mF+MHl0pbosvV38Ekv/rDiwSPsQySbQxESg6clF8Edu7X1KUbTjY8LdBed8GlD7VaNwfw5ScnaQyRg=="
}

if EventArgs.CurrentTabName == "Contract":
    #get Account ID
    accountId = ""
    if Quote.BillToCustomer:
        accountId = Quote.BillToCustomer.CrmAccountId

    contracts_qt = Quote.QuoteTables['Contracts']

    if accountId and contracts_qt.Rows.Count == 0:
        #load all contracts for associated to given AccountID
        getContractsURL =   "{baseURL}/{version}/agreements/{contractTypeName}?filter=ICMAccountID $eq '{accountId}'".format(baseURL=authParams["baseURL"], version=authParams["version"], contractTypeName=authParams["contractTypeName"], accountId =accountId)
        try:
            contracts = RestClient.Get(getContractsURL, headers)
            Trace.Write("Caltest: "+str(contracts))
            if contracts:
                contracts_qt.Rows.Clear()
                #Trace.Debug(contracts)
                for contract in contracts.Data:
                    contractId = contract.SysId.ToString()  #authParams["agreementId"]
                    Trace.Write("ContractID: {}".format(contractId))
                    if contractId:
                        quoteNumber = contract.ICMQuoteID.ToString()
                        #getLineItems
                        getLineItemsURL = "{baseURL}/{version}/agreements/{contractTypeName}/{agreementId}/associations/{associationName}?filter=ICMDescription $ne ''".format(baseURL=authParams["baseURL"], version=authParams["version"], contractTypeName=authParams["contractTypeName"], agreementId=contractId, associationName=authParams["associationName"])
                        try:
                            Trace.Write("getLineItemsURL: {}".format(getLineItemsURL))
                            lineItems = RestClient.Get(getLineItemsURL, headers)
                            for lineItem in lineItems.Data:
                                Trace.Debug(lineItem)
                                if not lineItem.ICMParentItemId.ToString() and lineItem.Name.ToString() == "Roche Lab Configuration":
                                    #addRow to Contracts table
                                    new_row = contracts_qt.AddNewRow()
                                    new_row.SetColumnValue('Customer_Reference', accountId)
                                    new_row.SetColumnValue('Contract_Number', quoteNumber)  #save here quote number
                                    new_row.SetColumnValue('Contract_Item_Number', int(lineItem.ICMQuoteItemId.ToString()))
                                    new_row.SetColumnValue('Contract_Item', lineItem.Name.ToString())  #Material
                                    new_row.SetColumnValue('Contract_Item_Description', lineItem.ICMDescription.ToString())
                                    new_row.SetColumnValue('Contract_Price', float(lineItem.ICMUnitPrice.Value))
                                    new_row.SetColumnValue('Contract_Instrument_Part_Nr_Reference', lineItem.SysId.ToString())
                                    new_row.SetColumnValue('agreement_id', contractId)
                                    new_row.SetColumnValue('Item_Status', lineItem.Status.ToString())
                                    
                
                                    
                        except Exception, e:
                            Trace.Write("(Load Contract Items Error: " + str(e) + " Line Number: " + str(sys.exc_traceback.tb_lineno))
                contracts_qt.Save()


                        
                    
                #if contract.Data and contract.Data.Id.ToString():
                    #Quote.GetCustomField("Icertis_AgreementId").Content = contract.Data.Id.ToString()
                    #authParams["agreementId"] =  contract.Data.Id.ToString()

                    #WorkflowContext.Message = "Sales Agreement has been created successfully."
                #if contract.Messages:
                    #WorkflowContext.Message = contract.Messages.ToString()
        except Exception,e:
            Trace.Write("(Load Account Contracts Error: " + str(e) + " Line Number: " + str(sys.exc_traceback.tb_lineno))
            #WorkflowContext.Message = "Create Sales Agreement Error: {}".format(e)
