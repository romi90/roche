contracts_qt = Quote.QuoteTables["Contracts"]

agreementID_toAmend = ""
#EventArgs.Table.Rows
itemID_helperArray = []
for item in Quote.Items:
    itemID_helperArray.append(item.RolledUpQuoteItem)



for row in contracts_qt.Rows:
    if row["Selector"]:
        quote_number = row["Contract_Number"]
        item_number = row["Contract_Item_Number"]
        agreementID_toAmend = row["agreement_id"]
        if Quote.GetCustomField("Icertis_AgreementId"):
            Quote.GetCustomField("Icertis_AgreementId").Content = agreementID_toAmend
        #original_pricing_json = RestClient.DeserializeJson(row["Contract_Original_Pricing"])
        #Trace.Write(original_pricing_json)
        row["Selector"] = False
        row["Added_To_Quote"] = True
        inlineProduct = ProductHelper.CreateProduct(quote_number, int(item_number))
        inlineProduct.AddToQuote()
        
contracts_qt.Save()
Trace.Write("Caltest agreementID_toAmend: "+agreementID_toAmend)
for item in Quote.Items:
    if not item.ParentItemGuid:
        if item.IsMainItem:
            for child in item.AsMainItem.Children:
                for grandchild in child.Children:
                    if  grandchild.PartNumber == "Instrument":
                        grandchild["ContractInstrument"].Value = grandchild.Description
                        grandchild["ContractInstrumentPrice"].Value = grandchild.ExtendedAmount
    if item.RolledUpQuoteItem not in itemID_helperArray:
        item["Item_Status"].Value = "Existing"
        item["AgreementId"].Value = agreementID_toAmend
        #item["AssociationId"].Value = 
Quote.Calculate()
Quote.Save()