import sys


authParams = {
    "baseURL": "https://poc42-business-api.icertis.com/api",
    "version": "v1.0", 
    "IcmAuthTokenName": "IcmAuthToken",
    "IcmAuthTokenValue": "N/lNTCx/xXoANOMgwl9AndIekQkwY3k7NcVvKYMC1hknxSXCiMqnctvKJ2JQd8lo7VfTIREl6S4FaKf3N0V335nDBesBqZP+Ka8SUFKwpc/bARc3QeR0T17o0Aq7qujfHhz39YHecaeRTO0HqMuhIsC6Hj5dg9MBl9h+CjNEBBXZfSYMdIX2/UfE9GZ2BTFPrQQdS8tTpWWjQQ8LE6nkaRtNnS/wmOPfjDJjGMCDkjw3gWDB7Or+BrLp+OlB+ngb30VXHLAzTOgvX/mvzpi0jhQi2eNhaOJ9hf/oqPfkhwDZRYVt6l6J6M/Dr3JgBOgF8OLxkQkNyxoSPSm4KUiWmA==$Xo7m+C1j939vJjzu3T0t13Fssaux6fEoam4NuxNVWCF11fHpPpTmC8GK7aa78zasgteow0FiVDVCXWW3cTIYsRHkl3eIxjHjQUURQT3FyIIKlkEhKkrzMJEou5IsdzP8cQrQexffdwM4uxNw4PW2QNp4Rdw3LzI+lwi2q1jR/2E=$Tn2BUqJB5vaEoz3qbabz5a43LjDouBroNPpX17kbtAc/eeyCKEMAVXrBPL9mGgMeS28hwoBjkIxyqJchGBaaWw==$qNFvZWD8mF+MHl0pbosvV38Ekv/rDiwSPsQySbQxESg6clF8Edu7X1KUbTjY8LdBed8GlD7VaNwfw5ScnaQyRg==", 
    "contractTypeName": "ICMSalesagreement",
    "guid": "9bbb1f86-d1da-4e83-ae75-4b945c73ae06",
    "amendmentId": "9bbb1f86-d1da-4e83-ae75-4b945c73ae06", 
    "associationName": "ICMProductsandPrices",
    "agreementId": "9bbb1f86-d1da-4e83-ae75-4b945c73ae06"
}

headers =  {
    #"Content-Type": "application/json", 
    "IcmAuthToken": "N/lNTCx/xXoANOMgwl9AndIekQkwY3k7NcVvKYMC1hknxSXCiMqnctvKJ2JQd8lo7VfTIREl6S4FaKf3N0V335nDBesBqZP+Ka8SUFKwpc/bARc3QeR0T17o0Aq7qujfHhz39YHecaeRTO0HqMuhIsC6Hj5dg9MBl9h+CjNEBBXZfSYMdIX2/UfE9GZ2BTFPrQQdS8tTpWWjQQ8LE6nkaRtNnS/wmOPfjDJjGMCDkjw3gWDB7Or+BrLp+OlB+ngb30VXHLAzTOgvX/mvzpi0jhQi2eNhaOJ9hf/oqPfkhwDZRYVt6l6J6M/Dr3JgBOgF8OLxkQkNyxoSPSm4KUiWmA==$Xo7m+C1j939vJjzu3T0t13Fssaux6fEoam4NuxNVWCF11fHpPpTmC8GK7aa78zasgteow0FiVDVCXWW3cTIYsRHkl3eIxjHjQUURQT3FyIIKlkEhKkrzMJEou5IsdzP8cQrQexffdwM4uxNw4PW2QNp4Rdw3LzI+lwi2q1jR/2E=$Tn2BUqJB5vaEoz3qbabz5a43LjDouBroNPpX17kbtAc/eeyCKEMAVXrBPL9mGgMeS28hwoBjkIxyqJchGBaaWw==$qNFvZWD8mF+MHl0pbosvV38Ekv/rDiwSPsQySbQxESg6clF8Edu7X1KUbTjY8LdBed8GlD7VaNwfw5ScnaQyRg=="
}

# headers = { 'authorization': RestClient.GetBasicAuthenticationHeader('icmsap-admin@icertis.com', 're5pect2021'), 'Content-Type': 'application/json'}      
# data = '[{"name": "SaaS Net New Revenue", "periodType": "month", "type": "Measurement", "value": { "amount": 100, "unitType": "USD" }}]'

contractData = {
    "Messages": None,
    "Data": {
        "ICMQuoteName": "Quote #{} for {} {}".format(Quote.CompositeNumber, Quote.BillToCustomer.CompanyName if Quote.BillToCustomer else "", Quote.RevisionNumber),
        "ICMClientName": "Salesforce", #drop down field, limited allowed options

        "ICMAfile": {
            "Content": "",
            "Extension": "",
            "Path": ""
        },
        "ICMMNPCode": {
            "UniqueValues": [],
            "DisplayValue": ""
        },
        "ICMADate": None,
        "ICMAEmail": "",
        "ICMProducts": [
            {
                "UniqueValues": ["ICMProductName", "Name"],
                "DisplayValue": "part,productname"
            }
        ],
        "ICMOrg": {
            "UniqueValues": [],
            "DisplayValue": ""
        },
        "ICMAString": "",
        "ICMMSA_ICMState": {
            "UniqueValues": [],
            "DisplayValue": ""
        },
        "ICMTerminatedParentId": "",
        "TypeOfPaper": "Own",
        "ICMADateTime": None,
        "ICMAUser": Quote.UserId.ToString(),
        "ICMServiceGroup": {
            "UniqueValues": [],
            "DisplayValue": ""
        },
        "ICMIsAssignment": False,
        "ICMCustomerCode": Quote.BillToCustomer.CustomerCode if Quote.BillToCustomer else "" ,
        "ICMCustomerName": Quote.BillToCustomer.CompanyName if Quote.BillToCustomer else "",
        "Name": Quote.CompositeNumber,
        "ICMOpportunityName": Quote.OpportunityName,
        "ICMSigner": "",
        "ICMAMultiselectChoice": [],
        "FilePath": {
            "Content": "",
            "Extension": "",
            "Path": ""
        },
        "ICMIsTermination": False,
        "ICMPin": {
            "UniqueValues": [],
            "DisplayValue": ""
        },
        "ICMOpportunityID": Quote.OpportunityId,
        "ICMQuoteID": Quote.CompositeNumber,
        "ICMAccountID": Quote.BillToCustomer.CrmAccountId if Quote.BillToCustomer else "",
        "ICMAccountType2": TagParserQuote.ParseString("<*CTX( Quote.Customer(BillTo).CustomField(Type) )*>"),

        "ICMANumber": None,
        "ICMABoolean": False,
        "ICMIsAssigned": False,
        "ICMAssignedParentId": "",
        "ICMATextArea": "",
        "ICMAgreementCode": "",
        "ICMTemplate": None,
        "ICMAChoice": "",
        "ICMMSA_ICMCity": {
            "UniqueValues": [],
            "DisplayValue": ""
        },
        "ICMAURL": "",
        "ICMContractValue": {
            "Value": Quote.Total.TotalNetPrice,
            "Currency": Quote.SelectedMarket.CurrencyCode
        },
        "ICMMSA_ICMCountry": {
            "UniqueValues": [],
            "DisplayValue": ""
        },
        "ICMACurrency": {
            "Value": Quote.SelectedMarket.CurrencyRate,
            "Currency": Quote.SelectedMarket.CurrencyCode
        },
        "ICMA": None,
        "ICMARichTextArea": "",
        "ICMLookupVarVP": {
            "UniqueValues": [],
            "DisplayValue": ""
        },
        "TypeOfContract": "Agreement",
        "ContractTypeName": "ICMMSA",
    },
    "HasMoreData": False,
    "PagingData": None
}

def getAmenmentBody(agrID):

    return {
        "Messages": None,
        "Data": {   
            "ICMTemplate": {
                "Name": ""            
            },
            "ICMContractValue": {
                "Value": 2222.0,
                "Currency": "USD"
            },
            "ICMUseAmendmentTemplate": False,        
            "ICMIsAssignment": False,
            "FilePath": {
                "Content": "",
                "Extension": "",
                "Path": ""
            },              
            "ICMRenewal": False,
            "ICMIsOrphanAmendment": False,
            "ICMParentEntityInstanceId": agrID,
            "TypeOfPaper": "Own",
            "TypeOfContract": "Agreement",        
            "ContractTypeName": "ICMSalesagreementAmendment",
            "ICMIsAmendment": True,
            "ICMOpportunityName": Quote.OpportunityName,
            "ICMOpportunityID": Quote.OpportunityId,
            "ICMQuoteID": Quote.CompositeNumber,
            "ICMCustomerCode": Quote.BillToCustomer.CustomerCode if Quote.BillToCustomer else "" ,
            "ICMCustomerName": Quote.BillToCustomer.CompanyName if Quote.BillToCustomer else "",
            "ICMAccountType2": TagParserQuote.ParseString("<*CTX( Quote.Customer(BillTo).CustomField(Type))*>"),
            "ICMAccountID": Quote.BillToCustomer.CrmAccountId if Quote.BillToCustomer else ""
        },
        "HasMoreData": False,
        "PagingData": None
}

def getProductData(item, customDescr):
    return {
        "ICMTemplate": None,
        "ICMAgreementCode": "",
        "Name": item.PartNumber,
        "ICMClientName": "",
        "FilePath": {
        "Content": "",
        "Extension": "",
        "Path": ""
        },
        "ICMVolume": item.Quantity.ToString(),
        "ICMProductName": item.Description,
        "ICMExternalId": "",
        "ICMUnitofMeasure": "",
        "ICMUnitPrice": {
        "Value": item.ExtendedAmountInMarket,
        "Currency":  Quote.SelectedMarket.CurrencyCode
        },
        "ICMItemStartDate": None,
        "ICMItemEndDate": None,
        "ICMItemStatus": item.Item_Status.Value,
        "ICMDescription": customDescr,
        "ICMParentItemId": item.ParentItemGuid, 
        "ICMQuoteItemId": item.QuoteItem,
        "TypeOfContract": "AssociatedDocument",
        "ContractTypeName": "ICMProductsandPrices"
    }

def getBulkProductData(listOfProductDataArray):
    return {
    "Messages": None,
    "Data": listOfProductDataArray,
    "HasMoreData": False,
    "PagingData": None
    }

#create or amend Contract
storedAgreementId = Quote.GetCustomField("Icertis_AgreementId")
if storedAgreementId is not None and storedAgreementId.Content != "":
    #amend existing contract
    authParams["agreementId"] = storedAgreementId.Content
    authParams["contractTypeName"] = "ICMSalesagreement"
    createOrUpdateContractURL = "{baseURL}/{version}/agreements/{contractTypeName}/{agreementId}/amendments".format(baseURL=authParams["baseURL"], version=authParams["version"], contractTypeName=authParams["contractTypeName"], agreementId=authParams["agreementId"])
    contractDataBody = getAmenmentBody(authParams["agreementId"])
else:
    #create new contract
    createOrUpdateContractURL =  "{baseURL}/{version}/agreements/{contractTypeName}/".format(baseURL=authParams["baseURL"], version=authParams["version"], contractTypeName=authParams["contractTypeName"])
    contractDataBody = contractData

#Trace.Write(RestClient.SerializeToJson(contractData))
#itemData = getItemData()
#contractData["Data"]["ICMProducts"] = itemData
try:
    Log.Write("Caltest createOrUpdateContractURL: "+createOrUpdateContractURL)
    Log.Write("Caltest contractDataBody: "+str(contractDataBody))
    contract = RestClient.Post(createOrUpdateContractURL, contractDataBody, headers)
    Trace.Write("Caltest create agreement: "+str(contract))
    if contract:
        #Trace.Debug(contract)
        if contract.Data and contract.Data.Id.ToString():
            Quote.GetCustomField("Icertis_AgreementId").Content = contract.Data.Id.ToString()
            authParams["agreementId"] =  contract.Data.Id.ToString()

            #WorkflowContext.Message = "Sales Agreement has been created successfully."
        #if contract.Messages:
            #WorkflowContext.Message = contract.Messages.ToString()
except Exception,e:
    Trace.Write("(Create Sales Agreement Error: " + str(e) + " Line Number: " + str(sys.exc_traceback.tb_lineno))
    #WorkflowContext.Message = "Create Sales Agreement Error: {}".format(e)

#pass Products and Prices:
# create item URL
Trace.Write("Create agreementId: "+str(authParams["agreementId"]))

if Quote.GetCustomField("Quote Type").Content == "Existing Business":
    authParams["contractTypeName"] = "ICMSalesagreementAmendment"
else:
    authParams["contractTypeName"] = "ICMSalesagreement"

Trace.Write("SHIV contractTypeName: "+str(authParams["contractTypeName"]))
    
createItemsURL = "{baseURL}/{version}/agreements/{contractTypeName}/{agreementId}/bulkassociations/{associationName}".format(baseURL=authParams["baseURL"], version=authParams["version"], contractTypeName=authParams["contractTypeName"], agreementId=authParams["agreementId"],associationName=authParams["associationName"])
bulkProductArrayHelper = []
for item in Quote.Items:
    #build custom description
    customDescr = ""
    if not item.ParentItemGuid:
        if item.IsMainItem:
            
            for child in item.AsMainItem.Children:
                if child.ProductTypeName == "Instruments":
                    customDescr += "\n{}: {}; Qty: {}; Net Price: {}".format(child.PartNumber, child.Description, child.Quantity, child.NetPriceInMarket)
                    for grandChild in child.Children:
                        if child.ProductTypeName == "Instruments":
                            customDescr += "\n  {}, {}; Qty: {}; Net Price: {}".format(grandChild.PartNumber, grandChild.Description, grandChild.Quantity, grandChild.NetPriceInMarket)
                            for grandGrandChild in grandChild.Children:
                                if grandChild.ProductTypeName == "Tests":
                                    customDescr += "\n  {}, {}; Qty: {}; Net Price: {}".format(grandGrandChild.PartNumber, grandGrandChild.Description, grandGrandChild.Quantity, grandGrandChild.NetPriceInMarket)

    #update produts json
    productData = getProductData(item, customDescr) 
    Trace.Write("ROMI: custom descr: {}".format(customDescr))
    bulkProductArrayHelper.append(productData)
#Trace.Write(   )
if bulkProductArrayHelper:
    bulkProductDataBody = getBulkProductData(bulkProductArrayHelper)
    Trace.Write("Caltest bulkProductDataBody: "+str(bulkProductDataBody))
    product = RestClient.Post(createItemsURL, bulkProductDataBody, headers)
    Trace.Write("Caltest create assoc: "+str(product))
    #if product.Data and product.Data.Id.ToString():
        #item["AssociationId"].Value = product.Data.Id.ToString()

Quote.Save(False)
